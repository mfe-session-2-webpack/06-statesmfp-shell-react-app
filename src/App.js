import React from 'react';
import SimpleAppBar from './app-bar/SimpleAppBar';
import AppEmitter from './emit-comp/Emitter';
import Divider from '@mui/material/Divider';

import './App.css';

function App() {
  return (
    <div className="App">
      <SimpleAppBar />
      <Divider />
      <div className="button-bar">
        <AppEmitter />
      </div>
    </div>
  );
}

export default App;
