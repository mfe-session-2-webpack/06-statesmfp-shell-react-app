import React from 'react';
import Skeleton from '@mui/material/Skeleton';
import Box from '@mui/material/Box';

import './Emitter.css';

const BuyButtons = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import('RemoteBuyButtons/BuyButtons')), 5000);
  });
});

function getSkeleton() {
  return (
    <>
      <Box component="span" sx={{ p: 2, border: '1px' }}>
        <Skeleton variant="rectangular" width={600} height={50} />
      </Box>
      <Skeleton variant="rounded" width="20%" />
    </>
  );
}

export default function AppEmitter() {
  return (
    <React.Suspense fallback={getSkeleton()}>
      <div>
        <BuyButtons />
      </div>
    </React.Suspense>
  );
}
