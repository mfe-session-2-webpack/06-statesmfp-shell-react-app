import React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Skeleton from '@mui/material/Skeleton';

const CartButton = React.lazy(() => import('RemoteCart/CartButton'));

export default function SimpleAppBar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            ReactShell
          </Typography>
          <React.Suspense
            fallback={
              <Skeleton variant="rectangular" width={600} height={210} />
            }
          >
            <div>
              <CartButton />
            </div>
          </React.Suspense>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
